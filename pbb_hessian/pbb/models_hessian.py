import math
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.distributions as td
from torchvision import datasets, transforms
from torchvision.utils import make_grid
from .hessian_utils import matrix_diag, gpu_memory, eigenthings_tensor_utils
from tqdm import tqdm, trange

def trunc_normal_(tensor, mean=0., std=1., a=-2., b=2.):
    # type: (Tensor, float, float, float, float) -> Tensor
    r"""Fills the input Tensor with values drawn from a truncated
    normal distribution. The values are effectively drawn from the
    normal distribution :math:`\mathcal{N}(\text{mean}, \text{std}^2)`
    with values outside :math:`[a, b]` redrawn until they are within
    the bounds. The method used works best if :math:`\text{mean}` is
    near the center of the interval.
    Args:
        tensor: an n-dimensional `torch.Tensor`
        mean: the mean of the normal distribution
        std: the standard deviation of the normal distribution
        a: the minimum cutoff value
        b: the maximum cutoff value
    Examples:
        >>> w = torch.empty(3, 5)
        >>> nn.init.trunc_normal_(w)
    """
    return _no_grad_trunc_normal_(tensor, mean, std, a, b)
    
    
   
def _no_grad_trunc_normal_(tensor, mean, std, a, b):
    # Method based on https://people.sc.fsu.edu/~jburkardt/presentations/truncated_normal.pdf
    def norm_cdf(x):
        # Computes standard normal cumulative distribution function
        return (1. + math.erf(x / math.sqrt(2.))) / 2.

    with torch.no_grad():
        # Get upper and lower cdf values
        l = norm_cdf((a - mean) / std)
        u = norm_cdf((b - mean) / std)

        # Fill tensor with uniform values from [l, u]
        tensor.uniform_(l, u)

        # Use inverse cdf transform from normal distribution
        tensor.mul_(2)
        tensor.sub_(1)

        # Ensure that the values are strictly between -1 and 1 for erfinv
        eps = torch.finfo(tensor.dtype).eps
        tensor.clamp_(min=-(1. - eps), max=(1. - eps))
        tensor.erfinv_()

        # Transform to proper mean, std
        tensor.mul_(std * math.sqrt(2.))
        tensor.add_(mean)

        # Clamp one last time to ensure it's still in the proper range
        tensor.clamp_(min=a, max=b)
        return tensor

class Gaussian(nn.Module):
    """Implementation of a Gaussian random variable, using softplus for
    the standard deviation and with implementation of sampling and KL
    divergence computation.

    Parameters
    ----------
    mu : Tensor of floats
        Centers of the Gaussian.

    rho : Tensor of floats
        Scale parameter of the Gaussian (to be transformed to std
        via the softplus function)

    device : string
        Device the code will run in (e.g. 'cuda')

    fixed : bool
        Boolean indicating whether the Gaussian is supposed to be fixed
        or learnt.

    """

    def __init__(self, mu, rho, device='cuda', fixed=False, fixed_rho=True):
        super().__init__()
        self.mu = nn.Parameter(mu, requires_grad=not fixed)
        self.rho = nn.Parameter(rho, requires_grad=not (fixed and fixed_rho))
        self.device = device

    @property
    def sigma(self):
        # Computation of standard deviation:
        # We use rho instead of sigma so that sigma is always positive during
        # the optimisation. Specifically, we use sigma = log(exp(rho)+1)
        return torch.log(1 + torch.exp(self.rho))

    def sample(self):
        # Return a sample from the Gaussian distribution
        epsilon = torch.randn(self.sigma.size()).to(self.device)
        return self.mu + self.sigma * epsilon

    def compute_kl(self, other):
        # Compute KL divergence between two Gaussians (self and other)
        # (refer to the paper)
        # b is the variance of priors
        b1 = torch.pow(self.sigma, 2)
        b0 = torch.pow(other.sigma, 2)

        term1 = torch.log(torch.div(b0, b1))
        term2 = torch.div(
            torch.pow(self.mu - other.mu, 2), b0)
        term3 = torch.div(b1, b0)
        kl_div = (torch.mul(term1 + term2 + term3 - 1, 0.5)).sum()
        return kl_div


class Laplace(nn.Module):
    """Implementation of a Laplace random variable, using softplus for
    the scale parameter and with implementation of sampling and KL
    divergence computation.

    Parameters
    ----------
    mu : Tensor of floats
        Centers of the Laplace distr.

    rho : Tensor of floats
        Scale parameter for the distribution (to be transformed
        via the softplus function)

    device : string
        Device the code will run in (e.g. 'cuda')

    fixed : bool
        Boolean indicating whether the distribution is supposed to be fixed
        or learnt.

    """

    def __init__(self, mu, rho, device='cuda', fixed=False):
        super().__init__()
        self.mu = nn.Parameter(mu, requires_grad=not fixed)
        self.rho = nn.Parameter(rho, requires_grad=not fixed)
        self.device = device

    @property
    def scale(self):
        # We use rho instead of sigma so that sigma is always positive during
        # the optimisation. We use sigma = log(exp(rho)+1)
        return torch.log(1 + torch.exp(self.rho))

    def sample(self):
        # Return a sample from the Laplace distribution
        # we do scaling due to numerical issues
        epsilon = (0.999*torch.rand(self.scale.size())-0.49999).to(self.device)
        result = self.mu - torch.mul(torch.mul(self.scale, torch.sign(epsilon)),
                                     torch.log(1-2*torch.abs(epsilon)))
        return result

    def compute_kl(self, other):
        # Compute KL divergence between two Laplaces distr. (self and other)
        # (refer to the paper)
        # b is the variance of priors
        b1 = self.scale
        b0 = other.scale
        term1 = torch.log(torch.div(b0, b1))
        aux = torch.abs(self.mu - other.mu)
        term2 = torch.div(aux, b0)
        term3 = torch.div(b1, b0) * torch.exp(torch.div(-aux, b1))

        kl_div = (term1 + term2 + term3 - 1).sum()
        return kl_div

class GaussianHessian(nn.Module):
    """Implementation of a Gaussian random variable, using softplus for
    the standard deviation and with implementation of sampling and KL
    divergence computation.

    Parameters
    ----------
    mu : Tensor of floats
        Centers of the Gaussian.

    rho : Tensor of floats
        Scale parameter of the Gaussian (to be transformed to std
        via the softplus function)

    device : string
        Device the code will run in (e.g. 'cuda')

    fixed : bool
        Boolean indicating whether the Gaussian is supposed to be fixed
        or learnt.

    """

    def __init__(self, mu, rho, outhessian_basis, autocorr_basis, outhessian_eigenvals, autocorr_eigenvals, device='cuda', fixed=False):
        super().__init__()
        self.mu = nn.Parameter(mu, requires_grad=not fixed)
        self.rho = nn.Parameter(rho, requires_grad=not fixed)
        self.outhessian_basis = outhessian_basis
        self.autocorr_basis = autocorr_basis

        self.outhessian_basis.requires_grad = False
        self.autocorr_basis.requires_grad = False
        
        self.outhessian_eigenvals = outhessian_eigenvals
        self.autocorr_eigenvals = autocorr_eigenvals
        self.device = device

    @property
    def sigma(self):
        # Computation of standard deviation:
        # We use rho instead of sigma so that sigma is always positive during
        # the optimisation. Specifically, we use sigma = log(exp(rho)+1)
        return F.softplus(self.rho)
        # return torch.log(1 + torch.exp(self.rho))
    
    @property
    def eigenvals(self):
        return self.outhessian_eigenvals.unsqueeze(-1).matmul(self.autocorr_eigenvals.unsqueeze(0))

    def to_hessian(self, vec):
        assert (vec.shape[0] == self.outhessian_basis.shape[0]) and (vec.shape[1] == self.autocorr_basis.shape[0]), (self.outhessian_basis.shape, vec.shape, self.autocorr_basis.shape)
        mv = torch.matmul(self.outhessian_basis, vec)
        res = torch.matmul(vec, self.autocorr_basis)
        return res
    
    def to_standard(self, vec):
        assert (vec.shape[0] == self.outhessian_basis.shape[0]) and (vec.shape[1] == self.autocorr_basis.shape[0]), (self.outhessian_basis.shape, vec.shape, self.autocorr_basis.shape)
        mtv = torch.matmul(self.outhessian_basis.t(), vec)
        res = torch.matmul(vec, self.autocorr_basis.t())
        return res
        # return (self.outhessian_basis.t()).matmul(vec).matmul(self.autocorr_basis.t())

    def sample(self):
        # Return a sample from the Gaussian distribution
        epsilon = torch.randn(self.sigma.size()).to(self.device)
        sigma = self.to_standard(self.sigma)
        return self.mu + sigma * epsilon

    def compute_kl(self, other):
        """
        Assert the "other" distribution is in standard basis

        If hessian is True, compute in Hessian basis

        Otherwise compute in standard basis
        """
        # Compute KL divergence between two Gaussians (self and other)
        # (refer to the paper)
        # b is the variance of priors
        other_sigma = other.sigma # Assert other is spherical distribution
        other_mu = self.to_hessian(other.mu)
        self_mu = self.to_hessian(self.mu)

        b1 = torch.pow(self.sigma, 2)
        b0 = torch.pow(other_sigma, 2)

        term1 = torch.log(torch.div(b0, b1))
        term2 = torch.div(torch.pow(self_mu - other_mu, 2), b0)
        term3 = torch.div(b1, b0)
        kl_div = (torch.mul(term1 + term2 + term3 - 1, 0.5)).sum()
        return kl_div



class Linear(nn.Module):
    """Implementation of a Linear layer (reimplemented to use
    truncated normal as initialisation for fair comparison purposes)

    Parameters
    ----------
    in_features : int
        Number of input features for the layer

    out_features : int
        Number of output features for the layer

    device : string
        Device the code will run in (e.g. 'cuda')

    """

    def __init__(self, in_features, out_features, device='cuda'):
        super().__init__()
        self.in_features = in_features
        self.out_features = out_features

        # Set sigma for the truncated gaussian of weights
        sigma_weights = 1/np.sqrt(in_features)

        # same initialisation as before for the prob layer
        self.weight = nn.Parameter(trunc_normal_(torch.Tensor(
            out_features, in_features), 0, sigma_weights, -2*sigma_weights, 2*sigma_weights), requires_grad=True)
        self.bias = nn.Parameter(torch.zeros(
            out_features), requires_grad=True)

    def forward(self, input):
        weight = self.weight
        bias = self.bias
        return F.linear(input, weight, bias)


class Lambda_var(nn.Module):
    """Class for the lambda variable included in the objective
    flambda

    Parameters
    ----------
    lamb : float
        initial value

    n : int
        Scaling parameter (lamb_scaled is between 1/sqrt(n) and 1)

    """

    def __init__(self, lamb, n):
        super().__init__()
        self.lamb = nn.Parameter(torch.tensor([lamb]), requires_grad=True)
        self.min = 1/np.sqrt(n)

    @property
    def lamb_scaled(self):
        # We restrict lamb_scaled to be between 1/sqrt(n) and 1.
        m = nn.Sigmoid()
        return (m(self.lamb) * (1-self.min) + self.min)

class ProbLinear(nn.Module):
    """Implementation of a Probabilistic Linear layer.

    Parameters
    ----------
    in_features : int
        Number of input features for the layer

    out_features : int
        Number of output features for the layer

    rho_prior : float
        prior scale hyperparmeter (to initialise the scale of
        the posterior)

    prior_dist : string
        string that indicates the type of distribution for the
        prior and posterior

    device : string
        Device the code will run in (e.g. 'cuda')

    init_layer : Linear object
        Linear layer object used to initialise the prior

    """

    def __init__(self, in_features, out_features, rho_prior, prior_dist='gaussian', device='cuda', init_layer=None, fix_prior_rho=True):
        super().__init__()
        self.in_features = in_features
        self.out_features = out_features

        # Set sigma for the truncated gaussian of weights
        sigma_weights = 1/np.sqrt(in_features)

        if init_layer:
            weights_mu_init = init_layer.weight
            bias_mu_init = init_layer.bias
        else:
            # Initialise distribution means using truncated normal
            weights_mu_init = trunc_normal_(torch.Tensor(
                out_features, in_features), 0, sigma_weights, -2*sigma_weights, 2*sigma_weights)
            bias_mu_init = torch.zeros(out_features)

        weights_rho_init = torch.ones(out_features, in_features) * rho_prior
        bias_rho_init = torch.ones(out_features) * rho_prior

        if prior_dist == 'gaussian':
            dist = Gaussian
        elif prior_dist == 'laplace':
            dist = Laplace
        else:
            raise RuntimeError(f'Wrong prior_dist {prior_dist}')

        self.bias = dist(bias_mu_init.clone(),
                         bias_rho_init.clone(), device=device, fixed=False)
        self.weight = dist(weights_mu_init.clone(),
                           weights_rho_init.clone(), device=device, fixed=False)
        self.weight_prior = dist(
            weights_mu_init.clone(), weights_rho_init.clone(), device=device, fixed=True, fixed_rho=fix_prior_rho)
        self.bias_prior = dist(
            bias_mu_init.clone(), bias_rho_init.clone(), device=device, fixed=True, fixed_rho=fix_prior_rho)

        self.kl_div = 0

    def forward(self, input, sample=False):
        if self.training or sample:
            # during training we sample from the model distribution
            # sample = True can also be set during testing if we
            # want to use the stochastic/ensemble predictors
            weight = self.weight.sample()
            bias = self.bias.sample()
        else:
            # otherwise we use the posterior mean
            weight = self.weight.mu
            bias = self.bias.mu
        if self.training:
            # sum of the KL computed for weights and biases
            self.kl_div = self.weight.compute_kl(self.weight_prior) + \
                self.bias.compute_kl(self.bias_prior)

        #print(self.weight_prior.rho)
        #print(self.weight_prior.rho.grad)
        return F.linear(input, weight, bias)

class ProbHessianLinear(nn.Module):
    """Implementation of a Probabilistic Linear layer with Hessian information.

    Parameters
    ----------
    in_features : int
        Number of input features for the layer

    out_features : int
        Number of output features for the layer

    rho_prior : float
        prior scale hyperparmeter (to initialise the scale of
        the posterior)

    prior_dist : string
        string that indicates the type of distribution for the
        prior and posterior

    device : string
        Device the code will run in (e.g. 'cuda')

    init_layer : Linear object
        Linear layer object used to initialise the prior

    autocorr : torch.Tensor
        The autocorrelation matrix

    outhessian : torch.Tensor
        The output Hessian matrix

    relu_follow : Boolean
        If the layer is followed with relu activation

    last_layer : Boolean
        If the layer is the last layer (for computing the outhessian based on the output)

    """

    def __init__(self, in_features, out_features, rho_prior, prior_dist='gaussian', device='cuda', init_layer=None, relu_follow=True, last_layer=False, use_hessian=True):
        super().__init__()
        self.in_features = in_features
        self.out_features = out_features

        self.relu_follow = relu_follow
        self.last_layer = last_layer
        self.use_hessian = use_hessian
        self.device = device

        # Set sigma for the truncated gaussian of weights
        sigma_weights = 1/np.sqrt(in_features)

        if init_layer:
            weights_mu_init = init_layer.weight
            bias_mu_init = init_layer.bias
        else:
            # Initialise distribution means using truncated normal
            weights_mu_init = trunc_normal_(torch.Tensor(
                out_features, in_features), 0, sigma_weights, -2*sigma_weights, 2*sigma_weights)
            bias_mu_init = torch.zeros(out_features)

        weights_rho_init = torch.ones(out_features, in_features) * rho_prior
        bias_rho_init = torch.ones(out_features) * rho_prior

        # placeholders for two kroneckor factored basis
        self.autocorr = torch.zeros((in_features, in_features)).to(device)
        self.autocorr_batch = 0

        self.outhessian_stack = None

        self.outhessian = torch.zeros((out_features, out_features)).to(device)
        self.outhessian_batch = 0

        self.activation_sigma = None

        if self.use_hessian:
            self.weight = GaussianHessian(weights_mu_init.clone(),
                            weights_rho_init.clone(),
                            torch.eye(out_features).to(device),
                            torch.eye(in_features).to(device),
                            torch.ones(out_features).to(device),
                            torch.ones(in_features).to(device),
                            device=device, fixed=False)
        else:
            self.weight = Gaussian(weights_mu_init.clone(),
                            weights_rho_init.clone(),
                            device=device, fixed=False)

        self.bias = Gaussian(bias_mu_init.clone(),
                         bias_rho_init.clone(),
                         device=device, fixed=False)

        self.weight_prior = Gaussian(weights_mu_init.clone(),
                                 weights_rho_init.clone(),
                                 device=device, fixed=True)

        self.bias_prior = Gaussian(bias_mu_init.clone(),
                              bias_rho_init.clone(),
                              device=device, fixed=True)

        self.outhessian_basis = self.weight.outhessian_basis
        self.autocorr_basis = self.weight.autocorr_basis
        self.outhessian_eigenvals = self.weight.outhessian_eigenvals
        self.autocorr_eigenvals = self.weight.autocorr_eigenvals
        
        self.kl_div = 0

    def hessian_forward(self, input, res):
        # record the autocorrelation matrix and activation for backprop of hessian
        x = input.detach()
        self.autocorr += torch.mean(torch.matmul(x.unsqueeze(-1), x.unsqueeze(-2)), axis=0)
        self.autocorr_batch += 1

        if self.last_layer:
            # Compute the A matrix when doing the forward propagation
            assert not self.relu_follow # the last layer should not be followed with relu for computing the outhessian
            
            p = F.softmax(res.detach(), dim=1)
            diag_p = matrix_diag(p)
            p_mat = p.unsqueeze(1)
            ppTs = torch.matmul(p_mat.transpose(1, 2), p_mat) # pylint: disable=no-member
            self.outhessian_stack = diag_p - ppTs
            # print(self.outhessian_stack.shape)

        if self.relu_follow:
            # record the activation if there relu applied for the output
            self.activation = matrix_diag((res >= 0).float()).detach()
        del x

    def forward(self, input, sample=False, hessian_fwd=False):
        if (self.training or sample) and not hessian_fwd:
            # during training we sample from the model distribution
            # sample = True can also be set during testing if we
            # want to use the stochastic/ensemble predictors
            weight = self.weight.sample()
            bias = self.bias.sample()
        else:
            # otherwise we use the posterior mean
            weight = self.weight.mu
            bias = self.bias.mu

        if self.training and not hessian_fwd:
            # sum of the KL computed for weights and biases
            self.kl_div = self.weight.compute_kl(self.weight_prior) + \
                          self.bias.compute_kl(self.bias_prior)
        
        # Compute the Linear output
        res = F.linear(input, weight, bias)
        
        if hessian_fwd:
            self.hessian_forward(input, res)

        return res

    def hessian_backward(self, next_layer):
        if not self.last_layer:
            if isinstance(next_layer.weight, nn.Module):
                W = next_layer.weight.mu.detach()
            else:
                W = next_layer.weight.detach()

            if self.relu_follow:
                U = W.matmul(self.activation)
            else:
                U = W
            self.outhessian_stack = U.transpose(1, 2).matmul(next_layer.outhessian_stack).matmul(U)
            
        self.outhessian += torch.mean(self.outhessian_stack, axis=0).detach()
        self.outhessian_batch += 1

    def reset_autocorr(self):
        self.autocorr *= 0
        self.autocorr_batch = 0

    def compute_autocorr(self, verbose=False):
        self.autocorr /= self.autocorr_batch
        if self.use_hessian:
            self.weight.autocorr_eigenvals, self.weight.autocorr_basis = eigenthings_tensor_utils(self.autocorr)
            if verbose:
                print("xxT {} {}".format(self.autocorr.shape, self.weight.autocorr_eigenvals[:20].cpu().tolist()))
    
    def reset_outhessian(self):
        self.outhessian *= 0
        self.outhessian_batch = 0
    
    def compute_outhessian(self, verbose=False):
        self.outhessian /= self.outhessian_batch
        if self.use_hessian:
            self.weight.outhessian_eigenvals, self.weight.outhessian_basis = eigenthings_tensor_utils(self.outhessian)
            if verbose:
                print("UTAU {} {}".format(self.outhessian.shape, self.weight.outhessian_eigenvals[:20].cpu().tolist()))

class ProbHessianFCN(nn.Module):
    """
    General Framework for Stochastic FCN with Hessian Information
    """
    def __init__(self, rho_prior=1, device='cuda', init_net=None):
        super().__init__()
        self.layers = []

    def compute_kl(self):
        # KL as a sum of the KL for each individual layer
        kl_div = [layer.kl_div for layer in self.layers]
        # print("KL:", kl_div)
        res = sum(kl_div)
        return res
        # return self.l1.kl_div + self.l2.kl_div + self.l3.kl_div + self.l4.kl_div

    def hessian_backward(self):
        self.layers[-1].hessian_backward(None)
        for i in range(2, 1 + len(self.layers)):
            self.layers[-i].hessian_backward(self.layers[-i + 1])

    def compute_hessian(self):
        for layer in self.layers:
            layer.compute_autocorr()
            layer.compute_outhessian()

    def reset_hessian(self):
        for layer in self.layers:
            layer.reset_autocorr()
            layer.reset_outhessian()
    
    def get_weight_posterior(self, verbose=False, store=False):
        res = {}
        min_sig, max_sig = 1e10, -1e10
        sigma_norm = 0
        for i, layer in enumerate(self.layers):
            sigma = layer.weight.sigma
            if verbose:
                sigma_norm += torch.norm(sigma)
                min_sig = min(min_sig, torch.min(sigma))
                max_sig = max(max_sig, torch.max(sigma))
            if store:
                res[i] = [layer.weight.mu.detach().cpu(),
                        layer.weight.sigma.detach().cpu()]
        if verbose:
            print("Post Sig\tNorm {:.5g}\tmin {:.5g}\tmax {:.5g}".format(sigma_norm, min_sig, max_sig))
        return res
    
    def get_weight_prior(self, verbose=False, store=False):
        res = {}
        min_sig, max_sig = 1e10, -1e10
        sigma_norm = 0
        for i, layer in enumerate(self.layers):
            sigma = layer.weight_prior.sigma
            if verbose:
                sigma_norm += torch.norm(sigma)
                min_sig = min(min_sig, torch.min(sigma))
                max_sig = max(max_sig, torch.max(sigma))
            if store:
                res[i] = [layer.weight_prior.mu.detach().cpu(),
                        layer.weight_prior.sigma.detach().cpu()]
        if verbose:
            print("PriorSig\tNorm {:.5g}\tmin {:.5g}\tmax {:.5g}".format(sigma_norm, min_sig, max_sig))
        return res

class ProbFCN(nn.Module):
    """
    General Framework for Stochastic FCN
    """

    def __init__(self):
        super().__init__()
        self.layers = []

    def compute_kl(self):
        kl_div = [layer.kl_div for layer in self.layers]
        res = sum(kl_div)
        return res
    
    def get_weight_posterior(self, verbose=False, store=False):
        res = {}
        min_sig, max_sig = 1e10, -1e10
        sigma_norm = 0
        for i, layer in enumerate(self.layers):
            sigma = layer.weight.sigma
            if verbose:
                sigma_norm += torch.norm(sigma)
                min_sig = min(min_sig, torch.min(sigma))
                max_sig = max(max_sig, torch.max(sigma))
            if store:
                res[i] = [layer.weight.mu.detach().cpu(),
                        layer.weight.sigma.detach().cpu()]
        if verbose:
            print("Post Sig\tNorm {:.5g}\tmin {:.5g}\tmax {:.5g}".format(sigma_norm, min_sig, max_sig))
        return res
    
    def get_weight_prior(self, verbose=False, store=False):
        res = {}
        min_sig, max_sig = 1e10, -1e10
        sigma_norm = 0
        for i, layer in enumerate(self.layers):
            sigma = layer.weight_prior.sigma
            if verbose:
                sigma_norm += torch.norm(sigma)
                min_sig = min(min_sig, torch.min(sigma))
                max_sig = max(max_sig, torch.max(sigma))
            if store:
                res[i] = [layer.weight_prior.mu.detach().cpu(),
                        layer.weight_prior.sigma.detach().cpu()]
        if verbose:
            print("PriorSig\tNorm {:.5g}\tmin {:.5g}\tmax {:.5g}".format(sigma_norm, min_sig, max_sig))
        return res

def output_transform(x, clamping=True, pmin=1e-4):
    """Computes the log softmax and clamps the values using the
    min probability given by pmin.

    Parameters
    ----------
    x : tensor
        output of the network

    clamping : bool
        whether to clamp the output probabilities

    pmin : float
        threshold of probabilities to clamp.
    """
    # lower bound output prob
    output = F.log_softmax(x, dim=1)
    if clamping:
        output = torch.clamp(output, np.log(pmin))
    return output


def trainNNet(net, optimizer, epoch, train_loader, device='cuda', verbose=False):
    """Train function for a standard NN (including CNN)

    Parameters
    ----------
    net : NNet/CNNet object
        Network object to train

    optimizer : optim object
        Optimizer to use (e.g. SGD/Adam)

    epoch : int
        Current training epoch

    train_loader: DataLoader object
        Train loader to use for training

    device : string
        Device the code will run in (e.g. 'cuda')

    verbose: bool
        Whether to print training metrics

    """
    # train and report training metrics
    net.train()
    total, correct, avgloss = 0.0, 0.0, 0.0
    for batch_id, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        net.zero_grad()
        output = net(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        pred = output.max(1, keepdim=True)[1]
        correct += pred.eq(target.view_as(pred)).sum().item()
        total += target.size(0)
        avgloss = avgloss + loss.detach()
    # show the average loss and KL during the epoch
    if verbose:
        print(f"-Epoch {epoch :.5f}, Train loss: {avgloss/batch_id :.5f}, Train err:  {1-(correct/total):.5f}")


def testNNet(net, test_loader, device='cuda', verbose=True):
    """Test function for a standard NN (including CNN)

    Parameters
    ----------
    net : NNet/CNNet object
        Network object to train

    test_loader: DataLoader object
        Test data loader

    device : string
        Device the code will run in (e.g. 'cuda')

    verbose: bool
        Whether to print test metrics

    """
    net.eval()
    correct, total = 0, 0.0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            outputs = net(data)
            loss = F.nll_loss(outputs, target)
            pred = outputs.max(1, keepdim=True)[1]
            correct += pred.eq(target.view_as(pred)).sum().item()
            total += target.size(0)
    print(f"Test loss: {loss :.5f}, Test err:  {1-(correct/total):.5f}")
    return 1-(correct/total)


def computeHessian(net, pbobj, train_loader):
    if hasattr(net, 'reset_hessian'):
        print("computing hessian")
        net.reset_hessian()
        net.eval()
        for batch_id, (data, target) in enumerate(train_loader):
            data, target = data.to(pbobj.device), target.to(pbobj.device)
            net.forward(data, hessian_fwd=True)
            net.hessian_backward()
        net.compute_hessian()
    else:
        print("Network cannot compute Hessian, skip")
    # gpu_memory()


def trainPNNet(net, optimizer, pbobj, epoch, train_loader, lambda_var=None, optimizer_lambda=None, verbose=False):
    """Train function for a probabilistic NN (including CNN)

    Parameters
    ----------
    net : ProbNNet/ProbCNNet object
        Network object to train

    optimizer : optim object
        Optimizer to use (e.g. SGD/Adam)

    pbobj : pbobj object
        PAC-Bayes inspired training objective to use for training

    epoch : int
        Current training epoch

    train_loader: DataLoader object
        Train loader to use for training

    lambda_var : Lambda_var object
        Lambda variable for training objective flamb

    optimizer_lambda : optim object
        Optimizer to use for the learning the lambda_variable

    device : string
        Device the code will run in (e.g. 'cuda')

    verbose: bool
        Whether to print test metrics

    """
    # torch.autograd.set_detect_anomaly(True)
    net.train()
    # variables that keep information about the results of optimising the bound
    avgerr, avgbound, avgkl, avgloss = 0.0, 0.0, 0.0, 0.0

    if pbobj.objective == 'flamb':
        lambda_var.train()
        # variables that keep information about the results of optimising lambda (only for flamb)
        avgerr_l, avgbound_l, avgkl_l, avgloss_l = 0.0, 0.0, 0.0, 0.0

    if pbobj.objective == 'bbb':
        clamping = False
    else:
        clamping = True

    for batch_id, (data, target) in enumerate(train_loader):
        data, target = data.to(pbobj.device), target.to(pbobj.device)
        net.zero_grad()
        # print("Begin", net.training)
        bound, kl, _, loss, err = pbobj.train_obj(
            net, data, target, lambda_var=lambda_var, clamping=clamping)
        bound.backward(retain_graph=True)
        avgbound += bound.item()
        avgkl += kl
        avgloss += loss.item()
        avgerr += err
        optimizer.step()

        # exit()
        if pbobj.objective == 'flamb':
            # for flamb we also need to optimise the lambda variable
            lambda_var.zero_grad()
            bound_l, kl_l, _, loss_l, err_l = pbobj.train_obj(
                net, data, target, lambda_var=lambda_var, clamping=clamping)
            bound_l.backward()
            optimizer_lambda.step()
            avgbound_l += bound_l.item()
            avgkl_l += kl_l
            avgloss_l += loss_l.item()
            avgerr_l += err_l

    if verbose:
        # show the average of the metrics during the epoch
        print(
            f"-Batch average epoch {epoch :.0f} results, Train obj: {avgbound/batch_id :.5f}, KL/n: {avgkl/batch_id :.5f}, NLL loss: {avgloss/batch_id :.5f}, Train 0-1 Error:  {avgerr/batch_id :.5f}")
        if pbobj.objective == 'flamb':
            print(
                f"-After optimising lambda: Train obj: {avgbound_l/batch_id :.5f}, KL/n: {avgkl_l/batch_id :.5f}, NLL loss: {avgloss_l/batch_id :.5f}, Train 0-1 Error:  {avgerr_l/batch_id :.5f}, last lambda value: {lambda_var.lamb_scaled.item() :.5f}")

    return avgloss/batch_id, avgerr/batch_id, avgkl/batch_id

def testStochastic(net, test_loader, pbobj, device='cuda'):
    """Test function for the stochastic predictor using a PNN

    Parameters
    ----------
    net : PNNet/PCNNet object
        Network object to test

    test_loader: DataLoader object
        Test data loader

    pbobj : pbobj object
        PAC-Bayes inspired training objective used during training

    device : string
        Device the code will run in (e.g. 'cuda')
    """
    # compute mean test accuracy
    net.eval()
    correct, cross_entropy, total = 0, 0.0, 0.0
    outputs = torch.zeros(test_loader.batch_size, pbobj.classes).to(device)
    with torch.no_grad():
        for batch_id, (data, target) in enumerate(test_loader):
            data, target = data.to(device), target.to(device)
            for i in range(len(data)):
                outputs[i, :] = net(data[i:i+1], sample=True,
                                    clamping=True, pmin=pbobj.pmin)
            cross_entropy += pbobj.compute_empirical_risk(
                outputs, target, bounded=True)
            pred = outputs.max(1, keepdim=True)[1]
            correct += pred.eq(target.view_as(pred)).sum().item()
            total += target.size(0)

    return cross_entropy/batch_id, 1-(correct/total)


def testPosteriorMean(net, test_loader, pbobj, verbose=True, device='cuda'):
    """Test function for the deterministic predictor using a PNN
    (uses the posterior mean)

    Parameters
    ----------
    net : PNNet/PCNNet object
        Network object to test

    test_loader: DataLoader object
        Test data loader

    pbobj : pbobj object
        PAC-Bayes inspired training objective used during training

    device : string
        Device the code will run in (e.g. 'cuda')

    """
    net.eval()
    correct, total = 0, 0.0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            outputs = net(data, sample=False, clamping=True, pmin=pbobj.pmin)
            cross_entropy = pbobj.compute_empirical_risk(
                outputs, target, bounded=True)
            pred = outputs.max(1, keepdim=True)[1]
            correct += pred.eq(target.view_as(pred)).sum().item()
            total += target.size(0)
    if verbose:
        print(f"Test loss: {cross_entropy :.5f}, Test err:  {1-(correct/total):.5f}")
    return cross_entropy, 1-(correct/total)


def testEnsemble(net, test_loader, pbobj, device='cuda', samples=100):
    """Test function for the ensemble predictor using a PNN

    Parameters
    ----------
    net : PNNet/PCNNet object
        Network object to test

    test_loader: DataLoader object
        Test data loader

    pbobj : pbobj object
        PAC-Bayes inspired training objective used during training

    device : string
        Device the code will run in (e.g. 'cuda')

    samples: int
        Number of times to sample weights (i.e. members of the ensembles)

    """
    net.eval()
    correct, cross_entropy, total = 0, 0.0, 0.0
    with torch.no_grad():
        for batch_id, (data, target) in enumerate(test_loader):
            data, target = data.to(device), target.to(device)
            outputs = torch.zeros(samples, test_loader.batch_size,
                                  pbobj.classes).to(device)
            for i in range(samples):
                outputs[i] = net(data, sample=True,
                                 clamping=True, pmin=pbobj.pmin)
            avgoutput = outputs.mean(0)
            cross_entropy = pbobj.compute_empirical_risk(
                avgoutput, target, bounded=True)
            pred = avgoutput.max(1, keepdim=True)[1]
            correct += pred.eq(target.view_as(pred)).sum().item()
            total += target.size(0)

    return cross_entropy/batch_id, 1-(correct/total)


def computeRiskCertificates(net, toolarge, pbobj, device='cuda', lambda_var=None, train_loader=None, whole_train=None):
    """Function to compute risk certificates and other statistics at the end of training

    Parameters
    ----------
    net : PNNet/PCNNet object
        Network object to test

    toolarge: bool
        Whether the dataset is too large to fit in memory (computation done in batches otherwise)

    pbobj : pbobj object
        PAC-Bayes inspired training objective used during training

    device : string
        Device the code will run in (e.g. 'cuda')

    lambda_var : Lambda_var object
        Lambda variable for training objective flamb

    train_loader: DataLoader object
        Data loader for computing the risk certificate (multiple batches, used if toolarge=True)

    whole_train: DataLoader object
        Data loader for computing the risk certificate (one unique batch, used if toolarge=False)

    """
    net.eval()
    with torch.no_grad():
        # a bit hacky, we load the whole dataset to compute the bound
        for data, target in whole_train:
            data, target = data.to(device), target.to(device)
            train_obj, kl, loss_ce_train, err_01_train, risk_ce, risk_01 = pbobj.compute_final_stats_risk(
                net, lambda_var=lambda_var, clamping=True, input=data, target=target)

    return train_obj, risk_ce, risk_01, kl, loss_ce_train, err_01_train
