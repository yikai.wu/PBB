import torch
import numpy as np
import torch.optim as optim
import torch.distributions as td
from torchvision import datasets, transforms
from torch.utils.data.sampler import SubsetRandomSampler
from .dp import OnDeviceDataLoader
from copy import deepcopy

def loaddataset(name):
    """Function to load the datasets (mnist and cifar10)

    Parameters
    ----------
    name : string
        name of the dataset ('mnist' or 'cifar10')

    """
    torch.manual_seed(7)

    if name == 'mnist':
        transform = transforms.Compose(
            [transforms.ToTensor(), transforms.Normalize((0.1307,), (0.3081,))])
        train = datasets.MNIST(
            '/usr/xtmp/CSPlus/VOLDNN/Datasets/MNIST', train=True, download=True, transform=transform)
        test = datasets.MNIST(
            '/usr/xtmp/CSPlus/VOLDNN/Datasets/MNIST', train=False, download=True, transform=transform)
    elif name == 'cifar10':
        transform = transforms.Compose(
            [transforms.ToTensor(),
             transforms.Normalize((0.4914, 0.4822, 0.4465),
                                  (0.2023, 0.1994, 0.2010)),
             ])
        train = datasets.CIFAR10(
            '/usr/xtmp/CSPlus/VOLDNN/Datasets/CIFAR10', train=True, download=True, transform=transform)
        test = datasets.CIFAR10(
            '/usr/xtmp/CSPlus/VOLDNN/Datasets/CIFAR10', train=False, download=True, transform=transform)
    else:
        raise RuntimeError(f'Wrong dataset chosen {name}')

    return train, test


def loadbatches(train, test, batch_size):
    """Function to load the batches for the dataset

    Parameters
    ----------
    train : torch dataset object
        train split
    
    test : torch dataset object
        test split 

    loader_kargs : dictionary
        loader arguments
    
    batch_size : int
        size of the batch

    prior : bool
        boolean indicating the use of a learnt prior (e.g. this would be False for a random prior)

    perc_train : float
        percentage of data used for training (set to 1.0 if not intending to do data scarcity experiments)

    perc_prior : float
        percentage of data to use for building the prior (1-perc_prior is used to estimate the risk)

    """

    train_loader = OnDeviceDataLoader(train, batch_size=batch_size, shuffle=True)
    test_loader = OnDeviceDataLoader(test, batch_size=batch_size, shuffle=True)
    set_val_bound = train_loader
    val_bound_one_batch = OnDeviceDataLoader(train, batch_size=len(train), shuffle=True)

    return train_loader, test_loader, set_val_bound, val_bound_one_batch
