import math
import numpy as np
import os, sys
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.distributions as td
from torchvision import datasets, transforms
from torchvision.utils import make_grid
from tqdm import tqdm, trange
from pbb.models_hessian import *
from pbb.bounds import PBBobj
from pbb import data

torch.autograd.set_detect_anomaly(True)

def runexp_hessian(name_data,
                   net_train,
                   objective,
                   sigma_prior,
                   pmin,
                   batch_size,
                   learning_rate,
                   momentum,
                   train_epochs,
                   hessian_epoch,
                   kl_penalty,
                   learning_rate_prior = 0.01,
                   momentum_prior=0.95,
                   seed=0,
                   delta=0.025,
                   delta_test=0.01,
                   mc_samples=1000, 
                   samples_ensemble=100,
                   initial_lamb=6.0,
                   fix_prior_rho=True,
                   device='cuda',
                   verbose=False,
                   verbose_test=False):
    """Run an experiment with PAC-Bayes inspired training objectives

    Parameters
    ----------
    name_data : string
        name of the dataset to use (check data file for more info)

    objective : string
        training objective to use

    model : torch.nn.Module
        could be cnn or fcn
    
    sigma_prior : float
        scale hyperparameter for the prior
    
    pmin : float
        minimum probability to clamp the output of the cross entropy loss
    
    learning_rate : float
        learning rate hyperparameter used for the optimiser

    momentum : float
        momentum hyperparameter used for the optimiser

    delta : float
        confidence parameter for the risk certificate
    
    delta_test : float
        confidence parameter for chernoff bound

    mc_samples : int
        number of monte carlo samples for estimating the risk certificate
        (set to 1000 by default as it is more computationally efficient, 
        although larger values lead to tighter risk certificates)

    samples_ensemble : int
        number of members for the ensemble predictor

    kl_penalty : float
        penalty for the kl coefficient in the training objective

    initial_lamb : float
        initial value for the lambda variable used in flamb objective
        (scaled later)
    
    train_epochs : int
        numer of training epochs for training

    prior_dist : string
        type of prior and posterior distribution (can be gaussian or laplace)

    verbose : bool
        whether to print metrics during training

    device : string
        device the code will run in (e.g. 'cuda')

    prior_epochs : int
        number of epochs used for learning the prior (not applicable if prior is rand)

    dropout_prob : float
        probability of an element to be zeroed.

    perc_train : float
        percentage of train data to use for the entire experiment (can be used to run
        experiments with reduced datasets to test small data scenarios)
    
    verbose_test : bool
        whether to print test and risk certificate stats during training epochs

    perc_prior : float
        percentage of data to be used to learn the prior

    batch_size : int
        batch size for experiments
    """

    # this makes the initialised prior the same for all bounds
    torch.manual_seed(seed)
    np.random.seed(0)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    # net_train = ProbHessianNNet3l_narrow

    train, test = data.loaddataset(name_data)
    rho_prior = math.log(math.exp(sigma_prior)-1.0)
    net = net_train(rho_prior, device=device, fix_prior_rho=fix_prior_rho).to(device)

    net_name = net.network_name
    exp_name = "{}_exp_epc{}_lr{}_mo{}_bt{}_fpr{}/run_seed{}".format(net_name, train_epochs, learning_rate, momentum, batch_size, fix_prior_rho, seed)
    print(exp_name)
    exp_path = os.path.join("./hessian_log/", exp_name)
    os.makedirs(exp_path, exist_ok=True)

    # initialise model
    train_loader, test_loader, val_bound, val_bound_one_batch = data.loadbatches(train, test, batch_size)

    toolarge = False
    train_size = len(train_loader.dataset)
    classes = len(train_loader.dataset.classes)

    bound = PBBobj(objective, pmin, classes, train_size, delta,
                    delta_test, mc_samples, kl_penalty, device)

    if objective == 'flamb':
        lambda_var = Lambda_var(initial_lamb, train_size).to(device)
        optimizer_lambda = optim.SGD(lambda_var.parameters(), lr=learning_rate, momentum=momentum)
    else:
        optimizer_lambda = None
        lambda_var = None

    optimizer = optim.SGD(net.parameters(), lr=learning_rate, momentum=momentum)
    print("Start Training")
    for epoch in range(train_epochs):
        if hessian_epoch >= 1:
            if epoch % hessian_epoch == 0:
                computeHessian(net, bound, train_loader)
                weight_post = net.get_weight_posterior()
                weight_post_name = 'epoch{}_weight_post.log'.format(epoch)
                torch.save(weight_post, os.path.join(exp_path, weight_post_name))

        t_loss, t_acc, kl = trainPNNet(net, optimizer, bound, epoch, train_loader, lambda_var, optimizer_lambda, verbose)
        v_loss, v_acc = testPosteriorMean(net, test_loader, bound, verbose)
        net.get_weight_prior(verbose=True)
        net.get_weight_posterior(verbose=True)
        
        if verbose_test and ((epoch+1) % 5 == 0):
            train_obj, risk_ce, risk_01, kl, loss_ce_train, loss_01_train = computeRiskCertificates(net, toolarge,
            bound, device=device, lambda_var=lambda_var, train_loader=val_bound, whole_train=val_bound_one_batch)

            stch_loss, stch_err = testStochastic(net, test_loader, bound, device=device)
            post_loss, post_err = testPosteriorMean(net, test_loader, bound, device=device)
            ens_loss, ens_err = testEnsemble(net, test_loader, bound, device=device, samples=samples_ensemble)

            print(f"***Checkpoint results***")         
            print(f"Objective, Dataset, Sigma, pmin, LR, momentum, LR_prior, momentum_prior, kl_penalty, dropout, Obj_train, Risk_CE, Risk_01, KL, Train NLL loss, Train 01 error, Stch loss, Stch 01 error, Post mean loss, Post mean 01 error, Ens loss, Ens 01 error, 01 error prior net, perc_train, perc_prior")
            print(f"{objective}, {name_data}, {sigma_prior :.5f}, {pmin :.5f}, {learning_rate :.5f}, {momentum :.5f}, {learning_rate_prior :.5f}, {momentum_prior :.5f}, {kl_penalty : .5f}, {dropout_prob :.5f}, {train_obj :.5f}, {risk_ce :.5f}, {risk_01 :.5f}, {kl :.5f}, {loss_ce_train :.5f}, {loss_01_train :.5f}, {stch_loss :.5f}, {stch_err :.5f}, {post_loss :.5f}, {post_err :.5f}, {ens_loss :.5f}, {ens_err :.5f}, {errornet0 :.5f}, {perc_train :.5f}, {perc_prior :.5f}")

    train_obj, risk_ce, risk_01, kl, loss_ce_train, loss_01_train = computeRiskCertificates(net, toolarge, bound, device=device,
    lambda_var=lambda_var, train_loader=val_bound, whole_train=val_bound_one_batch)

    stch_loss, stch_err = testStochastic(net, test_loader, bound, device=device)
    post_loss, post_err = testPosteriorMean(net, test_loader, bound, device=device)
    ens_loss, ens_err = testEnsemble(net, test_loader, bound, device=device, samples=samples_ensemble)

    print(f"***Final results***") 
    print(f"Objective, Dataset, Sigma, pmin, LR, momentum, LR_prior, momentum_prior, kl_penalty, dropout, Obj_train, Risk_CE, Risk_01, KL, Train NLL loss, Train 01 error, Stch loss, Stch 01 error, Post mean loss, Post mean 01 error, Ens loss, Ens 01 error, 01 error prior net, perc_train, perc_prior")
    print(f"{objective}, {name_data}, {sigma_prior :.5f}, {pmin :.5f}, {learning_rate :.5f}, {momentum :.5f}, {learning_rate_prior :.5f}, {momentum_prior :.5f}, {kl_penalty : .5f}, {train_obj :.5f}, {risk_ce :.5f}, {risk_01 :.5f}, {kl :.5f}, {loss_ce_train :.5f}, {loss_01_train :.5f}, {stch_loss :.5f}, {stch_err :.5f}, {post_loss :.5f}, {post_err :.5f}, {ens_loss :.5f}, {ens_err :.5f}, {errornet0 :.5f}, {perc_train :.5f}, {perc_prior :.5f}")


def count_parameters(model): 
    return sum(p.numel() for p in model.parameters() if p.requires_grad)
