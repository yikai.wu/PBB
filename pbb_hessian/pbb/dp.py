import torch
import pynvml
import math
from copy import deepcopy
from torch.utils.data import DataLoader
from queue import Queue
import datetime
from queue import PriorityQueue
import numpy as np
import time
import itertools

def log(info, print_log=True):
    if print_log:
        print("[{}]  {}".format(datetime.datetime.now(), info))

def timer(st, info='', timer_on=True):
    if timer_on:
        print("[{}]  {} {}".format(datetime.datetime.now(), datetime.datetime.now() - st, info))

def est(st, progress, info=''):
    time_diff = datetime.datetime.now() - st
    est = (1 - progress) * time_diff / progress
    print("[{}]  {:.3g}%\t EST: {}".format(datetime.datetime.now(), progress * 100, str(est)[:-4]))

def get_time_seed():
    return int(str(datetime.datetime.now())[-4:])

def get_tensor_size(tensor):
    return tensor.element_size() * tensor.nelement()

def get_tensor_dict_size(tensor_dict, p=False):
    ret = 0
    for k in tensor_dict:
        ret += get_tensor_size(tensor_dict[k])
    if p:
        print("{:.4g} G".format(ret / (2 ** 30)))
    return ret

def gpu_memory(print_out=True):
    try:
        pynvml.nvmlInit()
        handle = pynvml.nvmlDeviceGetHandleByIndex(0)
        mem_info = pynvml.nvmlDeviceGetMemoryInfo(handle)
        time.sleep(0.2)
        if print_out:
            log("{} {:.5g}% {:.4g}G".format(pynvml.nvmlDeviceGetName(handle).decode("utf-8"), 100 * mem_info.used / mem_info.total, mem_info.total/(2**30)))
        return mem_info.used, mem_info.total
    except:
        return None, None

def empty_cache(device):
    if device == 'cpu':
        return
    else:
        torch.cuda.empty_cache()

def matrix_diag(diagonal):
    N = diagonal.shape[-1]
    shape = diagonal.shape[:-1] + (N, N)
    device, dtype = diagonal.device, diagonal.dtype
    result = torch.zeros(shape, dtype=dtype, device=device) # pylint: disable=no-member
    indices = torch.arange(result.numel(), device=device).reshape(shape) # pylint: disable=no-member
    indices = indices.diagonal(dim1=-2, dim2=-1)
    result.view(-1)[indices] = diagonal
    return result

def cartesian_product(arrays):
    la = len(arrays)
    dtype = np.find_common_type([a.dtype for a in arrays], [])
    arr = np.empty([len(a) for a in arrays] + [la], dtype=dtype)
    for i, a in enumerate(np.ix_(*arrays)):
        arr[..., i] = a
    return arr.reshape(-1, la)

def kmax_argsort(a, b, k, return_vals=False):

    q = PriorityQueue()
    la, lb = len(a), len(b)
    assert k <= la * lb
    
    q.put((- a[0] * b[0], (0, 0)))
    vals, args = [], []
    args_set = set((0, 0))

    for _ in range(k):
    
        val, (ia, ib) = q.get()
        vals.append(-val)
        args.append((ia, ib))
    
        if ia + 1 < la:
            if (ia + 1, ib) not in args_set:
                args_set.add((ia + 1, ib))
                q.put((- a[ia + 1] * b[ib], (ia + 1, ib)))
    
        if ib + 1 < lb:
            if (ia, ib + 1) not in args_set:
                args_set.add((ia, ib + 1))
                q.put((- a[ia] * b[ib + 1], (ia, ib + 1)))
    
    if return_vals:
        return args, vals
    else:
        return args

class OnDeviceDataLoader():

    def __init__(self, dataset, batch_size=128, device='cpu', shuffle=False, num_workers=4, ds_crop=1, **kwargs):
        self.dataset = dataset
        self.device = device
        self.batchsize = batch_size
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.dataset_crop = 1
        self.mem_approx()

        self.inds_raw, self.inps_raw, self.labels_raw = self.load_data()
        self.inds, self.labels, self.inps = self.set_dscrop(ds_crop)

        self.batch_inds = self.batch_inds_sep()
        self.batch_inds_q = Queue()

        self.batch_count = math.ceil(self.dataset_size() / self.batchsize)
        log('On-Device dataset initialized')
    
    def mem_approx(self):
        sample_dl = DataLoader(self.dataset, batch_size=1)
        batch_input, batch_label = iter(sample_dl).next()
        sample_size = get_tensor_size(batch_input) + get_tensor_size(batch_label)
        gig_size = sample_size * len(self.dataset) / (2 ** 30)
        log('Dataset size in memory: {:.4g}G'.format(gig_size))
        return gig_size

    def set_batchsize(self, batchsize=100):
        self.batchsize = batchsize
        self.batch_inds = self.batch_inds_sep()
        self.batch_inds_q = Queue()
        self.batch_count = math.ceil(self.dataset_size() / self.batchsize)
        log("Modified batchsize to {}".format(self.batchsize))
        return
    
    def set_dscrop(self, ds_crop=1):

        if ds_crop == 1:
            return self.inds_raw, self.labels_raw, self.inps_raw
        
        assert ds_crop > 0 and ds_crop < 1
        ds_crop_ind = int(len(self.labels_raw) * ds_crop)
        
        inds = deepcopy(self.inds_raw[:ds_crop_ind])
        labels = self.labels_raw[:ds_crop_ind]
        inps = self.inps_raw[:ds_crop_ind]
        log("setting dscrop to {}".format(ds_crop))

        return inds, labels, inps
    
    def __len__(self):
        return self.batch_count

    def dataset_size(self):
        return len(self.inps)

    def load_data(self):
        sample_dl = DataLoader(self.dataset, batch_size=2048, num_workers=4)
        inds = np.arange(len(self.dataset))
        inps_stack, labels_stack = [], []
        for (inp, label) in sample_dl:
            inps_stack.append(inp.to(self.device))
            labels_stack.append(label.to(self.device))
        
        inps, labels = torch.cat(inps_stack), torch.cat(labels_stack) # pylint: disable=no-member
        return inds, inps, labels

    def batch_inds_sep(self):
        batch_inds = []
        for i in np.arange(0, self.dataset_size(), self.batchsize):
            batch_inds.append([i, i + self.batchsize])
        batch_inds[-1][-1] = self.dataset_size()
        return batch_inds
    
    def init_batch_inds_q(self):
        self.batch_inds_q = Queue()
        for x in self.batch_inds:
            self.batch_inds_q.put(x)
    
    def __next__(self):
        if self.batch_inds_q.empty():
            raise StopIteration
        batch_marks = self.batch_inds_q.get()
        batch_inds = list(range(batch_marks[0], batch_marks[1]))
        inp = self.inps[self.inds[batch_inds]]
        labels = self.labels[self.inds[batch_inds]]

        data = [inp, labels]
        return data
    
    def __iter__(self):
        self.init_batch_inds_q()
        if self.shuffle:
            np.random.shuffle(self.inds)
        return self
    
    next = __next__
