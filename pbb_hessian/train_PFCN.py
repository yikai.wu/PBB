import torch
from pbb.utils import runexp_hessian
from networks.PHFCN import PHFCN_200x2
from networks.PFCN import PFCN_200x2

DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(torch.cuda.is_available())

DATASET = 'mnist'
OBJECTIVE = 'bbb'
net_train = PFCN_200x2

BATCH_SIZE = 250
TRAIN_EPOCHS = 1000

SIGMAPRIOR = 0.03
PMIN = 1e-5
KL_PENALTY = 0.1

LEARNING_RATE = 0.001
MOMENTUM = 0.95

HESSIAN_EPOCH = -1
RANDOMSEED = 0

DELTA = 0.025
DELTA_TEST = 0.01

# note the number of MC samples used in the paper is 150.000, which usually takes a several hours to compute
MC_SAMPLES = 1000
ENSEMBLE_SAMPLES = 10

FIX_PRIOR_RHO = True
# note all of these running examples have different settings!
# runexp_hessian('mnist', 'bbb', PRIOR, 'fcn', SIGMAPRIOR, PMIN, LEARNING_RATE, MOMENTUM, LEARNING_RATE_PRIOR, MOMENTUM_PRIOR, delta=DELTA, delta_test=DELTA_TEST, mc_samples=MC_SAMPLES, train_epochs=TRAIN_EPOCHS, device=DEVICE, perc_train=1.0, verbose=True, dropout_prob=0.2, kl_penalty=KL_PENALTY)

runexp_hessian(DATASET,
               net_train,
               OBJECTIVE,
               SIGMAPRIOR,
               PMIN,
               BATCH_SIZE,
               LEARNING_RATE,
               MOMENTUM,
               TRAIN_EPOCHS,
               HESSIAN_EPOCH,
               KL_PENALTY,
               RANDOMSEED, 
               delta=DELTA,
               delta_test=DELTA_TEST,
               mc_samples=MC_SAMPLES,
               samples_ensemble=ENSEMBLE_SAMPLES,
               verbose=True,
               verbose_test=False,
               fix_prior_rho=FIX_PRIOR_RHO
               )