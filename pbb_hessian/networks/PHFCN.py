from pbb.models_hessian import *

class PHFCN_200x2(ProbHessianFCN):

    def __init__(self, rho_prior, prior_dist='gaussian', device='cuda', init_net=None):
        super().__init__()

        self.network_name = "PH200x2"
        self.l1 = ProbHessianLinear(28*28, 200, rho_prior, prior_dist=prior_dist,
                             device=device, init_layer=init_net.l1 if init_net else None)
        self.l2 = ProbHessianLinear(200, 200, rho_prior, prior_dist=prior_dist,
                             device=device, init_layer=init_net.l2 if init_net else None)
        self.l3 = ProbHessianLinear(200, 10, rho_prior, prior_dist=prior_dist,
                             device=device, init_layer=init_net.l3 if init_net else None, relu_follow=False, last_layer=True)
        self.layers = [self.l1, self.l2, self.l3]

    def forward(self, x, sample=False, hessian_fwd=False, clamping=True, pmin=1e-4):
        x = x.view(-1, 28*28)
        x = F.relu(self.l1(x, sample, hessian_fwd))
        x = F.relu(self.l2(x, sample, hessian_fwd))
        x = output_transform(self.l3(x, sample, hessian_fwd), clamping, pmin)
        return x