from pbb.models_hessian import *

class PFCN_200x2(ProbFCN):

    def __init__(self, rho_prior, prior_dist='gaussian', device='cuda', init_net=None, fix_prior_rho=True):
        super().__init__()

        self.network_name = "P200x2"
        self.l1 = ProbLinear(28*28, 200, rho_prior, prior_dist=prior_dist,
                             device=device, init_layer=init_net.l1 if init_net else None, fix_prior_rho=fix_prior_rho)
        self.l2 = ProbLinear(200, 200, rho_prior, prior_dist=prior_dist,
                             device=device, init_layer=init_net.l2 if init_net else None, fix_prior_rho=fix_prior_rho)
        self.l3 = ProbLinear(200, 10, rho_prior, prior_dist=prior_dist,
                             device=device, init_layer=init_net.l3 if init_net else None, fix_prior_rho=fix_prior_rho)
        self.layers = [self.l1, self.l2, self.l3]

    def forward(self, x, sample=False, clamping=True, pmin=1e-4):
        x = x.view(-1, 28*28)
        x = F.relu(self.l1(x, sample))
        x = F.relu(self.l2(x, sample))
        x = output_transform(self.l3(x, sample), clamping, pmin)
        return x